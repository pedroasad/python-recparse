.. vim: et: sw=3: ts=3: tw=100:
   vim: filetype=rst:
   vim: spell: spelllang=en:

recparse documentation
======================

.. toctree::
   :maxdepth: 2

   Project's README <README>
   tutorial
   package
   ROADMAP
   CHANGELOG

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
