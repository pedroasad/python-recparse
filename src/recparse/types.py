# -*- coding: utf-8 -*-
#
# recparse - Declarative parsing of records in binary files
# Copyright (C) 2018  Pedro Asad
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.
#
# If you have any questions or suggestions regarding this software, visit
# <http://gitlab.com/psa-exe/recparse/>.

"""\
Primitive data types
====================
"""

from recparse import PrimitiveType


Float32 = PrimitiveType(struct_format='f')
Int32 = PrimitiveType(struct_format='i')
Uint16 = PrimitiveType(struct_format='H')
